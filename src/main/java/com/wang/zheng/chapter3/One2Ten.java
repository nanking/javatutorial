/**
 * @description:for 循环
 * @author wang.zheng
 * @version 1.0
 * @date 2021-01-28 13:49
 */

package com.wang.zheng.chapter3;

public class One2Ten {
    public static void main(String[] args) {
        for(int i=0;i<10;i++){
            System.out.println(i);
        }
    }
}
