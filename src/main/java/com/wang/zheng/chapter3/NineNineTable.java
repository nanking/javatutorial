/**
 * @description:九九乘法表
 * @author wang.zheng
 * @version 1.0
 * @date 2021-01-28 13:51
 */

package com.wang.zheng.chapter3;

public class NineNineTable {
    public static void main(String[] args) {
        for(int i=0;i<10;i++){
            for(int j=0;j<10;j++){
                System.out.printf("%d*%d=%2d ", i, j, i * j);
            }
            System.out.println();
        }
    }
}
