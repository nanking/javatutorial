/**
 * @description: 获取不同类型的取值范围
 * @author wang.zheng
 * @version 1.0
 * @date 2021-01-27 12:04
 */

package com.wang.zheng.chapter3;

import java.awt.*;

public class Range {
    public static void main(String[] args) {
        // %n 换行符
        int num = 3;
        double num3 = 0.2;
        float num2 = (float)(1.0-0.8);
        System.out.printf("%d ~ %d%n",Byte.MIN_VALUE,Byte.MAX_VALUE);
        System.out.printf("%f%n", num2);
        System.out.printf("%f = %f ? %b%n",num2,num3,num2==num3);
        System.out.printf("%d ~ %d\n",Integer.MIN_VALUE,Integer.MAX_VALUE);
        System.out.printf("%d ~ %d\n",Long.MIN_VALUE,Long.MAX_VALUE);
        System.out.println(String.format("%d ~ %d", Byte.MIN_VALUE, Byte.MAX_VALUE));
        System.out.println(String.format("%d是否为偶数？%c%n", num, num % 2 ==0 ? '是':'否'));
    }
}
