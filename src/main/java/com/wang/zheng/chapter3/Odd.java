/**
 * @description:Odd
 * @author wang.zheng
 * @version 1.0
 * @date 2021-01-27 18:29
 */

package com.wang.zheng.chapter3;

public class Odd {
    public static void main(String[] args) {
        int input = 10;
        int remain = input %2;
        //余数为1 就是奇数
        if(remain == 1){
            System.out.printf("%d 为奇数%n",input);
        }
        else{ //
            System.out.printf("%d 为偶数%n",input);
             }
    }
}
