/**
 * @description:数据复制
 * @author wang.zheng
 * @version 1.0
 * @date 2021-02-03 14:36
 */

package com.wang.zheng.chapter4;

import java.util.Arrays;

public class arrayCopy {
    public static void main(String[] args) {
        int [] scores1 = {88, 81, 74, 68, 78, 76, 77, 85, 95, 93};
        int[] scores2 = Arrays.copyOf(scores1, scores1.length);
        for(int score:scores2){
            System.out.printf("%3d",score);
        }
        scores1[2] = 85;
        System.out.println("");
        //不影响score1
        for(int score:scores1){
            System.out.printf("%3d", score);
        }
    }
}