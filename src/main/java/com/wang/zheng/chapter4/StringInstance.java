/**
 * @description:字符串特性
 * @author wang.zheng
 * @version 1.0
 * @date 2021-03-02 14:10
 */

package com.wang.zheng.chapter4;

public class StringInstance {
    public static void main(String[] args) {
        char[] name = {'w', 'a', 'n','g','z','h','e','n','g'};
        String name1 = new String(name);
        String name2 = new String(name);
        String name3 = "wang"+"zheng";
        String name4 = "wangzheng";
        System.out.printf("%s，%s%n",name1,name2);
        System.out.println(name1.equals(name2));
        System.out.println(name1 == name2);
        System.out.println(name1 == name2);
        System.out.println(name3 == name4);
        System.out.println("wangzheng"=="wangzheng");
    }
}
