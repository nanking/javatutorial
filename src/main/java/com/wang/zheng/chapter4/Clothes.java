/**
 * @description:类复制
 * @author wang.zheng
 * @version 1.0
 * @date 2021-02-03 15:47
 */

package com.wang.zheng.chapter4;

public class Clothes {
    String color;
    char size;
    Clothes(String color, char size){
        this.color = color;
        this.size = size;
    }
}
