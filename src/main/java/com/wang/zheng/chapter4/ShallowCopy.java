/**
 * @description:ShallowCopy
 * @author wang.zheng
 * @version 1.0
 * @date 2021-02-03 15:51
 */

package com.wang.zheng.chapter4;

public class ShallowCopy {
    public static void main(String[] args) {
        Clothes[] c1 = {new Clothes("red", 'L'), new Clothes("bluw", 'M')};
        Clothes[] c2 = new Clothes[c1.length];
        for(int i=0;i<c1.length;i++){
            c2[i] = c1[i];
        }
        c1[0].color = "yellow";
        System.out.println(c2[0].color);
    }
}
