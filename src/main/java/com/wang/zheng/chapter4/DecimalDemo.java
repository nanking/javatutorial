/**
 * @description:DecimalDemo
 * @author wang.zheng
 * @version 1.0
 * @date 2021-01-29 10:44
 */

package com.wang.zheng.chapter4;

import java.math.BigDecimal;


public class DecimalDemo {
    public static void main(String[] args) {
        System.out.printf("%.2f - %.2f = %.2f%n", 1.0, 0.2, 1.0 - 0.2);
        float a = 1.0f;
        float b = 0.2f;
        System.out.println(a-b);
        BigDecimal operand1 = new BigDecimal("1.0");
        BigDecimal operand2 = new BigDecimal("0.2");
        BigDecimal result = operand1.subtract(operand2);
        System.out.println(result);
    }
}
