/**
 * @description:字符串拼接
 * @author wang.zheng
 * @version 1.0
 * @date 2021-03-02 14:56
 */

package com.wang.zheng.chapter4;
import java.lang.StringBuilder;
public class StringBuilderDemo {
    public static void main(String[] args) {
        for(int i=1;i<100;i++){
            System.out.printf("%d+",i);
        }
        System.out.print("100");

        StringBuilder builder = new StringBuilder();
        for(int i=1; i<100; i++){
            builder.append(i).append('+');
        }
        builder.append(100);
        System.out.println(builder.toString());
    }
}
