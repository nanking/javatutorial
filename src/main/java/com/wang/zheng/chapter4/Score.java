/**
 * @description:循环数组
 * @author wang.zheng
 * @version 1.0
 * @date 2021-02-01 16:34
 */

package com.wang.zheng.chapter4;

public class Score {
    public static void main(String[] args) {
        int [] scores = {88, 81, 74, 68, 78, 76, 77, 85, 95, 93};
        for(int i=0;i<scores.length;i++){
            System.out.printf("第%d个学生分数%d%n", i, scores[i]);
        }
    }
}
