/**
 * @description:值比较
 * @author wang.zheng
 * @version 1.0
 * @date 2021-02-01 16:12
 */

package com.wang.zheng.chapter4;

public class IntEquals {
    public static void main(String[] args) {
        Integer int1 = 100;
        Integer int2 = 100;
        Integer int3 = 200;
        Integer int4 = 200;
        System.out.println("Q:100==100 ?");
        if (int1 == int2) {
            System.out.println("A:100==100");
        } else {
            System.out.println("A:100!=100");
        }
        System.out.println("Q:200==200 ?");
        if (int3 == int4) {
            System.out.println("A:200==200");
        } else {
            System.out.println("A:200!=200");
        }
        System.out.println("Q:100 equal 100 ?");
        if (int1.equals(int2)) {
            System.out.println("A:100 equal 100");
        } else {
            System.out.println("A:100 not equal 100");
        }
        System.out.println("Q:200 equal 200 ?");
        if (int3.equals(int4)) {
            System.out.println("A:100 equal 100");
        } else {
            System.out.println("A:100 not equal 100");
        }
    }
}
